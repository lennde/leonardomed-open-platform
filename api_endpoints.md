**ICD10 Diagnoses**

API Endpoint: **/public_api_diagnoses_icd10**

Returns all ICD10 diagnosis codes, in the format {icd_code[string], name[string]}